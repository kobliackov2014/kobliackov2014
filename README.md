# Kobliackov2014

The version of my CV for GitLab

A junior programmer with less than a year of commercial development experience. I have quite good programming skills on the platform .Net С#. I mainly worked on casual games on Unity, and also did mobile projects on Xamarin.

## My latest projects 
* 📚🏫 - [Master's thesis](https://gitlab.com/kobliackov2014/magistrdiplom)
* 📚🏫 - [Bachelor's thesis](https://gitlab.com/kobliackov2014/PCVR)
* 🎮📚 - [Side scroller](https://gitlab.com/kobliackov2020/infinite-runner)
* 🎮📚 - [GeometryDashClone](https://gitlab.com/kobliackov2014/geometrydashclone)

#### Legend
🎮 - Game /
📚 - Training project /
🏫 - Graduate 

****

### Key points
* Currently working IT Magic Lab
* Write simple projects in my free time

### Educations
📘  Place of study: Taganrog Technological Institute of Southern Federal University, Taganrog<br />
Specialty: Information systems and Technologies<br />
Level: Bachelor<br />
 
📕🎓 Place of study: Taganrog Technological Institute of Southern Federal University, Taganrog <br />
      Specialty: Machine learning and Big Data analysis technologies<br />
      Level: Master's Degree<br />

### 🛠 Technical Stack
* C# languages
* Unity, Xamarin
* GitHub / GitLab
